#include "stdafx.h"
#include "Node.h"
#include <iostream>
#include <iomanip>

MinHeap Node::heap;
Node* Node::map = new Node[NUM_INDEX];

void Node::compute()
{
	Node* start = &map[0];
	start->visited = true;
	pushAdjToHeap( start );

	while ( !heap.array.empty() )
	{
		if ( heap.GetMin().target->visited )
		{
			heap.DeleteMin();
		}
		else
		{
			visit( start );
			pushAdjToHeap( start );
		}
	}
}

void Node::visit( Node* &start )
{
	start = heap.GetMin().target;
	start->distance = heap.GetMin().distance;
	start->visited = true;
	heap.DeleteMin();			
}

void Node::pushAdjToHeap( Node* start )
{
	for ( int edgeIndex = 0; edgeIndex < start->edges.size(); edgeIndex++ )
	{
		int targetIndex = start->edges[edgeIndex].target;
		if ( !map[targetIndex - 1].visited )
		{
			int targetDist = start->distance + start->edges[edgeIndex].distance;
			heap.Insert( nextVisit( &map[targetIndex - 1], targetDist ) );
		}
	}
}

nextVisit::nextVisit( Node* target, int dist ):target( target ), distance( dist )
{
}

Edge::Edge()
{
}

Edge::Edge( int index, int distance )
{
	target = index;
	this->distance = distance;
}

Edge::~Edge()
{

}

Node::Node():index( 0 ), distance( 0 ), visited( false )
{
	static int currentIndex = 1;
	index = currentIndex;
	currentIndex++;
}

Node::Node( int index ): index( index )
{
}

Node::~Node()
{
}

void Node::addEdge( int target, int distance )
{
	edges.push_back( Edge( target, distance ) );
}

void Node::print()
{
	cout << "#" << setw(3) << index;
	cout << " >>" << setw( 5 ) << distance;
	//for ( int i = 0; i < edges.size(); i++ )
	//{
	//	cout << setw( 3 ) << edges[i].target << "," << edges[i].distance;
	//}
	cout << endl;
}

bool operator<( const nextVisit &lhs, const nextVisit &rhs )
{
	return lhs.distance < rhs.distance;
}

bool operator>( const nextVisit &lhs, const nextVisit &rhs )
{
	return lhs.distance > rhs.distance;
}
