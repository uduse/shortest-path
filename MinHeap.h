#ifndef _BINARY_HEAP_H_
#define _BINARY_HEAP_H_

//#include "vector.h"
#include <vector>
#include "Node.h"

using namespace std;

struct nextVisit;
class Node;

using namespace std;

class MinHeap
{
private:


public:
	void BubbleDown( int index );
	void BubbleUp( int index );
	void Heapify();
	//MinHeap( int* array, int length );
	//MinHeap( const vector<Packet>& vector );
	
	MinHeap();
	vector<nextVisit> array;


	void Insert( nextVisit newPacket );
	nextVisit GetMin();

	void DeleteMin();
	void print()const;
	bool isEmpty();
};

#endif
