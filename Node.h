#pragma once
#include <vector>
#include "MinHeap.h"

//#define NUM_INDEX 7
#define NUM_INDEX 200

using namespace std;

class Node;
class MinHeap;

struct nextVisit
{
	Node* target;
	int distance;

	nextVisit( Node* target, int dist );

	friend bool operator<( const nextVisit &lhs, const nextVisit &rhs );
	friend bool operator>( const nextVisit &lhs, const nextVisit &rhs );
};

class Edge
{
public:
	Edge();
	Edge( int index, int distance );
	~Edge();

	int target;
	int source;
	int distance;
private:
};

class Node
{
public:
	Node();
	Node( int index );
	~Node();

	void addEdge( int target, int distance );
	void print();

	static MinHeap heap;
	static Node* map;
	static void compute();

	static void visit( Node* &start );
	static void pushAdjToHeap( Node* start );

private:
	int index;
	int distance;
	bool visited;
	vector<Edge> edges;

};
