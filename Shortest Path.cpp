// Shortest Path.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iosfwd>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Node.h"

using namespace std;



int _tmain( int argc, _TCHAR* argv[] )
{
	int index;
	fstream input( "dijkstraData.txt" );
	//fstream input( "1.txt" );
	string s;

	while ( getline( input, s ) )
	{
		istringstream strm( s );

		strm >> index;

		int target;
		char comma;
		int dist;

		while ( strm >> target >> comma >> dist )
		{
			Node::map[index - 1].addEdge( target, dist );
		}
	}

	Node::compute();


	for ( int i = 0; i < NUM_INDEX; i++ )
	{
		/*if ( i == 7 || i == 37 || i == 59 || i == 82 || i == 99
			 || i == 115 || i == 133 || i == 165 || i == 188 || i == 197 )
			 {*/
			Node::map[i - 1].print();
		//}
	}

	return 0;
}

