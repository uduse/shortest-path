#include "stdafx.h"
#include "MinHeap.h"
#include <iostream>


MinHeap::MinHeap()
{
}

void MinHeap::Heapify()
{
	int length = array.size();
	for ( int i = length - 1; i >= 0; --i )
	{
		BubbleDown( i );
	}
}

void MinHeap::BubbleDown( int index )
{
	int length = array.size();
	int leftChildIndex = 2 * index + 1;
	int rightChildIndex = 2 * index + 2;
	//cout << "L: " << leftChildIndex << "\tR: " << rightChildIndex << endl;

	if ( leftChildIndex >= length )
		return; //index is a leaf

	int minIndex = index;

	if ( array[index] > array[leftChildIndex] )
	{
		minIndex = leftChildIndex;
		//cout << "Turn Left, Min Index : " << minIndex << endl;
	}

	if ( ( rightChildIndex < length ) && ( array[minIndex] > array[rightChildIndex] ) )
	{
		minIndex = rightChildIndex;

	}

	if ( minIndex != index )
	{
		//need to swap
		nextVisit temp = array[index];
		array[index] = array[minIndex];
		array[minIndex] = temp;
		BubbleDown( minIndex );
	}
}

void MinHeap::BubbleUp( int index )
{
	if ( index == 0 )
		return;

	int parentIndex = ( index - 1 ) / 2;

	if ( array[parentIndex] > array[index] )
	{
		//need to swap
		nextVisit temp = array[parentIndex];
		array[parentIndex] = array[index];
		array[index] = temp;
		BubbleUp( parentIndex );
	}
}

void MinHeap::Insert( nextVisit newPacket )
{
	int length = array.size();

	array.push_back( newPacket );

	BubbleUp( array.size() - 1 );
}

nextVisit MinHeap::GetMin()
{
	return array.front();
}

void MinHeap::DeleteMin()
{
	int length = array.size();

	if ( length == 0 )
	{
		return;
	}

	swap( array.front(), array.back() );

	array.pop_back();
	BubbleDown( 0 );

	//return true;
}

void MinHeap::print()const
{
	cout << endl;
	for ( int pos = 0; pos < array.size(); pos++ )
	{
		array[pos].target->print();
	}
	cout << endl;
}


